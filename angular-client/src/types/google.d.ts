declare namespace google {
	namespace accounts {
		namespace id {
			function initialize(params: any): void
			function renderButton(params: any, config: any): void
			const prompt: any
		}
	}
}
