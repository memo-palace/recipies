import { Component, inject, Input, OnInit } from '@angular/core'
import { Recipe } from 'src/app/models/recipe'
import { RecipeShowService } from './recipe-show.service'

@Component({
	selector: 'app-recipe-show',
	templateUrl: './recipe-show.component.html',
	styleUrls: ['./recipe-show.component.scss'],
	providers: [RecipeShowService],
})
export class RecipeShowComponent implements OnInit {
	@Input() id!: string

	private _recipeShowService = inject(RecipeShowService)

	get recipe(): Recipe | null {
		return this._recipeShowService.recipe()
	}

	ngOnInit(): void {
		this._recipeShowService.get(this.id)
	}

	delete(): void {
		this._recipeShowService.delete()
	}
}
