import { TestBed } from '@angular/core/testing'

import { RecipeShowService } from './recipe-show.service'

describe('RecipeShowService', () => {
	let service: RecipeShowService

	beforeEach(() => {
		TestBed.configureTestingModule({})
		service = TestBed.inject(RecipeShowService)
	})

	it('should be created', () => {
		expect(service).toBeTruthy()
	})
})
