import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RecipeShowComponent } from './recipe-show.component'
import { MatListModule } from '@angular/material/list'
import { MatIconModule } from '@angular/material/icon'
import { MatButtonModule } from '@angular/material/button'
import { AppRoutingModule } from 'src/app/app-routing.module'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { MatDialogModule } from '@angular/material/dialog'

@NgModule({
	declarations: [RecipeShowComponent],
	imports: [
		CommonModule,
		MatListModule,
		MatIconModule,
		MatButtonModule,
		AppRoutingModule,
		MatProgressSpinnerModule,
		MatDialogModule,
	],
})
export class RecipeShowModule {}
