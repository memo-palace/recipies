import { inject, Injectable, signal, WritableSignal } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { MatSnackBar } from '@angular/material/snack-bar'
import { Router } from '@angular/router'
import { ErrorService } from 'src/app/core/services/error.service'
import { HttpService } from 'src/app/core/services/http.service'
import { LoaderService } from 'src/app/core/services/loader.service'
import { Recipe } from 'src/app/models/recipe'
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component'

@Injectable()
export class RecipeShowService {
	private _httpService = inject(HttpService)
	private _loaderService = inject(LoaderService)
	private _dialog = inject(MatDialog)
	private _errorService = inject(ErrorService)
	private _snackBar = inject(MatSnackBar)
	private _router = inject(Router)

	recipe: WritableSignal<Recipe | null> = signal(null)

	get(id: string): void {
		this._loaderService.setIsLoading(true)

		this._httpService.get<Recipe>(`recipes/${id}`).subscribe({
			next: recipe => {
				this.recipe.set(recipe)
				this._loaderService.setIsLoading(false)
			},
		})
	}

	delete(): void {
		this._dialog
			.open(ConfirmDialogComponent, {
				data: {
					title: 'Delete recipe',
					message: 'Are you sure you want to delete this recipe?',
				},
			})
			.afterClosed()
			.subscribe(confirmed => {
				if (confirmed) {
					this._httpService.delete(`recipes/${this.recipe()?.id}`).subscribe({
						next: () => {
							this._snackBar.open('Recipe was deleted', 'OK')
							this._router.navigate(['recipes'])
						},
						error: () => {
							this._errorService.handleError()
						},
					})
				}
			})
	}
}
