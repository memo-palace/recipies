import { inject, Injectable } from '@angular/core'
import { FormBuilder, Validators, FormArray } from '@angular/forms'
import { MatSnackBar } from '@angular/material/snack-bar'
import { ErrorService } from 'src/app/core/services/error.service'
import { HttpService } from 'src/app/core/services/http.service'
import { LoaderService } from 'src/app/core/services/loader.service'
import { CreateRecipeRequest, Recipe, UpdateRecipeRequest } from 'src/app/models/recipe'

@Injectable({
	providedIn: 'root',
})
export class RecipeHandleService {
	private _httpService = inject(HttpService)
	private _fb = inject(FormBuilder)
	private _errorService = inject(ErrorService)
	private _snackBar = inject(MatSnackBar)
	private _loaderService = inject(LoaderService)

	private _id!: string

	form = this._fb.group({
		title: ['', Validators.required],
		ingredients: this._fb.array([]),
		ingredient: [''],
		description: [''],
	})

	get ingredients() {
		return this.form.controls['ingredients'] as FormArray
	}

	init(id: string): void {
		this._clearForm()

		if (id) {
			this._prefillForm(id)
		}
	}

	create(): void {
		const controls = this.form.controls
		const recipe = {
			title: controls.title.value,
			ingredients: this.ingredients.value.map((i: { name: string }) => i.name),
			description: controls.description.value,
		} as CreateRecipeRequest

		this._httpService.post('recipes', recipe).subscribe({
			next: () => {
				this._clearForm()
				this._snackBar.open('Recipe was created', 'OK')
			},
			error: () => {
				this._errorService.handleError()
			},
		})
	}

	update(): void {
		const controls = this.form.controls
		const recipe = {
			title: controls.title.value,
			ingredients: this.ingredients.value.map((i: { name: string }) => i.name),
			description: controls.description.value,
		} as UpdateRecipeRequest

		this._httpService.put(`recipes/${this._id}`, recipe).subscribe({
			next: () => {
				this._snackBar.open('Recipe was updated', 'OK')
			},
			error: () => {
				this._errorService.handleError()
			},
		})
	}

	addIngredient(): void {
		const ingredientGroup = this._fb.group({
			name: [this.form.controls.ingredient.value],
		})
		this.ingredients.push(ingredientGroup)

		this.form.controls.ingredient.reset()
	}

	removeIngredient(index: number): void {
		this.ingredients.removeAt(index)
	}

	private _clearForm(): void {
		this.form.reset()
		this.ingredients.clear()
	}

	private _prefillForm(id: string): void {
		this._loaderService.setIsLoading(true)
		this._id = id
		this._httpService.get<Recipe>(`recipes/${id}`).subscribe({
			next: recipe => {
				const controls = this.form.controls
				controls.title.patchValue(recipe.title)
				controls.description.patchValue(recipe.description)
				recipe.ingredients?.forEach(ingredient => {
					const ingredientGroup = this._fb.group({
						name: [ingredient],
					})
					this.ingredients.push(ingredientGroup)
				})
				this._loaderService.setIsLoading(false)
			},
			error: () => {
				this._loaderService.setIsLoading(false)
				this._errorService.handleError()
			},
		})
	}
}
