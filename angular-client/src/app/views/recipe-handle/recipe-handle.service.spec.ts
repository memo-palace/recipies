import { TestBed } from '@angular/core/testing';

import { RecipeHandleService } from './recipe-handle.service';

describe('RecipeHandleService', () => {
  let service: RecipeHandleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecipeHandleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
