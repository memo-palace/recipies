import { Component, inject, Input, OnInit } from '@angular/core'
import { FormGroup, FormArray } from '@angular/forms'
import { RecipeHandleService } from './recipe-handle.service'

@Component({
	selector: 'app-recipe-handle',
	templateUrl: './recipe-handle.component.html',
	styleUrls: ['./recipe-handle.component.scss'],
})
export class RecipeHandleComponent implements OnInit {
	@Input() id!: string

	private _recipeHandleService = inject(RecipeHandleService)

	get form(): FormGroup {
		return this._recipeHandleService.form
	}

	get ingredients(): FormArray {
		return this._recipeHandleService.ingredients
	}

	ngOnInit(): void {
		this._recipeHandleService.init(this.id)
	}

	create(): void {
		this._recipeHandleService.create()
	}

	update(): void {
		this._recipeHandleService.update()
	}

	addIngredient(): void {
		this._recipeHandleService.addIngredient()
	}

	removeIngredient(index: number): void {
		this._recipeHandleService.removeIngredient(index)
	}
}
