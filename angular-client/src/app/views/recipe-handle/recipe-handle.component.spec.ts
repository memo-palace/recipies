import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeHandleComponent } from './recipe-handle.component';

describe('RecipeHandleComponent', () => {
  let component: RecipeHandleComponent;
  let fixture: ComponentFixture<RecipeHandleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RecipeHandleComponent]
    });
    fixture = TestBed.createComponent(RecipeHandleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
