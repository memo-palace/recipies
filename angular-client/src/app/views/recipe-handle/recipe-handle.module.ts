import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RecipeHandleComponent } from './recipe-handle.component'
import { ReactiveFormsModule } from '@angular/forms'
import { MatButtonModule } from '@angular/material/button'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatIconModule } from '@angular/material/icon'
import { MatInputModule } from '@angular/material/input'

@NgModule({
	declarations: [RecipeHandleComponent],
	imports: [
		CommonModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		MatInputModule,
		MatIconModule,
		MatButtonModule,
	],
})
export class RecipeHandleModule {}
