import { Component, inject, OnInit } from '@angular/core'
import { Recipe } from '../../models/recipe'
import { RecipesService } from './recipes.service'

@Component({
	selector: 'app-recipes',
	templateUrl: './recipes.component.html',
	styleUrls: ['./recipes.component.scss'],
})
export class RecipesComponent implements OnInit {
	private _recipesService = inject(RecipesService)

	get recipes(): Recipe[] {
		return this._recipesService.recipes()
	}

	ngOnInit(): void {
		this._recipesService.getRecipes().subscribe()
	}
}
