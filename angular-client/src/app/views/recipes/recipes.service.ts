import { inject, Injectable, signal, WritableSignal } from '@angular/core'
import { Observable, tap } from 'rxjs'
import { HttpService } from 'src/app/core/services/http.service'
import { Recipe } from '../../models/recipe'

@Injectable()
export class RecipesService {
	private _httpService = inject(HttpService)

	recipes: WritableSignal<Recipe[]> = signal([])

	getRecipes(): Observable<Recipe[]> {
		return this._httpService.get<Recipe[]>('recipes', { hello: 'there' }).pipe(
			tap(recipes => {
				this.recipes.set(recipes)
			})
		)
	}
}
