import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RecipesComponent } from './recipes.component'
import { MatCardModule } from '@angular/material/card'
import { RecipesService } from './recipes.service'
import { AppRoutingModule } from 'src/app/app-routing.module'

@NgModule({
	declarations: [RecipesComponent],
	exports: [RecipesComponent],
	imports: [CommonModule, MatCardModule, AppRoutingModule],
	providers: [RecipesService],
})
export class RecipesModule {}
