import { inject, Injectable } from '@angular/core'
import { HttpService } from 'src/app/core/services/http.service'

@Injectable()
export class GoogleSignInService {
	private _httpService = inject(HttpService)

	init(): void {
		this.onLoad()
	}

	onLoad(): void {
		google.accounts.id.initialize({
			client_id: '196255232717-23u1m90k3pn08dc2fumv4h7b2gsr0528.apps.googleusercontent.com',
			callback: this.handleCredentialResponse.bind(this),
		})
		google.accounts.id.renderButton(
			document.getElementById('google-sign-in'),
			{ theme: 'outline', size: 'large' } // customization attributes
		)
		google.accounts.id.prompt() // also display the One Tap dialog
	}

	handleCredentialResponse(response: { credential: string }) {
		console.log('Encoded JWT ID token: ' + response.credential)
		this._httpService
			.post('auth/login', { token: response.credential, rememberMe: true })
			.subscribe({
				next: () => {
					console.log('logged in')
				},
				error: () => {
					console.log('error')
				},
			})
	}
}
