import { AfterContentInit, Component, inject, OnInit } from '@angular/core'
import { GoogleSignInService } from './google/google-sign-in.service'

@Component({
	selector: 'app-sign-in',
	templateUrl: './sign-in.component.html',
	styleUrls: ['./sign-in.component.scss'],
	providers: [GoogleSignInService],
})
export class SignInComponent implements AfterContentInit {
	private _googleSignInService = inject(GoogleSignInService)

	ngAfterContentInit(): void {
		this._googleSignInService.init()
	}
}
