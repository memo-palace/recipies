export interface Recipe {
	id: string
	title: string
	ingredients: string[]
	description: string
	created: Date
	modified: Date
}

export interface CreateRecipeRequest {
	title: string
	ingredients: string[]
	description: string
}

export interface UpdateRecipeRequest {
	id: string
	title: string
	ingredients: string[]
	description: string
}
