import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { RecipeHandleComponent } from './views/recipe-handle/recipe-handle.component'
import { RecipeShowComponent } from './views/recipe-show/recipe-show.component'
import { RecipesComponent } from './views/recipes/recipes.component'
import { SignInComponent } from './views/sign-in/sign-in.component'

const routes: Routes = [
	{ path: 'sign-in', component: SignInComponent },
	{ path: 'recipes', component: RecipesComponent },
	{ path: 'create-recipe', component: RecipeHandleComponent },
	{ path: 'recipes/:id', component: RecipeShowComponent },
	{ path: 'edit-recipe/:id', component: RecipeHandleComponent },
	{ path: '', redirectTo: '/sign-in', pathMatch: 'full' },
]

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			bindToComponentInputs: true,
		}),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
