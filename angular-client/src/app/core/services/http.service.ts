import { HttpClient, HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { environment } from '../../../environments/environment'

@Injectable({
	providedIn: 'root',
})
export class HttpService {
	private _baseUrl: string

	constructor(private _http: HttpClient) {
		this._baseUrl = environment.apiUrl
	}

	get<T>(url: string, queryParams?: object): Observable<T> {
		const options = queryParams ? { params: this._getHttpParams(queryParams) } : {}

		return this._http.get<T>(`${this._baseUrl}${url}`, options)
	}

	post(url: string, payload: any): Observable<any> {
		return this._http.post(`${this._baseUrl}${url}`, payload)
	}

	put(url: string, payload: any): Observable<any> {
		return this._http.put(`${this._baseUrl}${url}`, payload)
	}

	delete(url: string): Observable<any> {
		return this._http.delete(`${this._baseUrl}${url}`)
	}

	private _getHttpParams(queryParams: any): HttpParams {
		return Object.keys(queryParams).reduce((previous, current) => {
			return previous.set(current, queryParams[current])
		}, new HttpParams())
	}
}
