import { Injectable, signal, WritableSignal } from '@angular/core'

@Injectable({
	providedIn: 'root',
})
export class LoaderService {
	readonly isLoading: WritableSignal<boolean> = signal(false)

	setIsLoading(isLoading: boolean): void {
		this.isLoading.set(isLoading)
	}
}
