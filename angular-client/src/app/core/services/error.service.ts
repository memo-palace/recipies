import { inject, Injectable } from '@angular/core'
import { MatSnackBar } from '@angular/material/snack-bar'

@Injectable({
	providedIn: 'root',
})
export class ErrorService {
	private _snackBar = inject(MatSnackBar)

	handleError(message?: string): void {
		message = message ?? 'Something went wrong'

		this._snackBar.open(message!, 'OK')
	}
}
