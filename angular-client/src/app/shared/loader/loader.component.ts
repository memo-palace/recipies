import { Component, inject } from '@angular/core'
import { LoaderService } from 'src/app/core/services/loader.service'

@Component({
	selector: 'app-loader',
	templateUrl: './loader.component.html',
	styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent {
	private _loaderService = inject(LoaderService)

	get isLoading(): boolean {
		return this._loaderService.isLoading()
	}
}
