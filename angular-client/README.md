# AngularClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.3.

## Setup local environment

### Install Node/NPM

Use NVM (Node Version Manager) to easily switch between different Node/NPM versions.

1. curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
2. nvm install 18
3. nvm use 18

### Install Angular CLI

1. sudo npm install -g @angular/cli@latest

### Run project

1. Navigate to the angular-client root folder in your terminal.
2. Run: ng s

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
