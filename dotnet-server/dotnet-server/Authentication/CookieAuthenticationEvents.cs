using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

using dotnet_server.Services;

namespace dotnet_server.Authentication;

public class CustomCookieAuthenticationEvents : CookieAuthenticationEvents
{
    private readonly UserService _userService;
    private readonly ILogger<CustomCookieAuthenticationEvents> _logger;

    public CustomCookieAuthenticationEvents(UserService userService, ILogger<CustomCookieAuthenticationEvents> logger)
    {
        _userService = userService;
        _logger = logger;
    }

    public override async Task ValidatePrincipal(CookieValidatePrincipalContext context)
    {
        var userPrincipal = context.Principal;

        var lastChanged = (from c in userPrincipal?.Claims
                           where c.Type == "LastChanged"
                           select c.Value).FirstOrDefault();


        _logger.LogInformation($"Validating user {userPrincipal?.Identity?.Name} with last changed {lastChanged}");

        if (string.IsNullOrEmpty(lastChanged) || string.IsNullOrEmpty(userPrincipal?.Identity?.Name) ||
            await _userService.AuthValid(userPrincipal.Identity.Name, DateTime.Parse(lastChanged)) == false)
        {
            context.RejectPrincipal();

            await context.HttpContext.SignOutAsync(
                CookieAuthenticationDefaults.AuthenticationScheme);
        }
    }
}