using dotnet_server.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace dotnet_server.Services;

public class UserService
{
    private readonly IMongoCollection<User> _userCollection;

    public UserService(
        IOptions<RecipesDatabaseSettings> recipesDatabaseSettings)
    {
        var mongoClient = new MongoClient(
            recipesDatabaseSettings.Value.ConnectionString);

        var mongoDatabase = mongoClient.GetDatabase(
            recipesDatabaseSettings.Value.DatabaseName);

        _userCollection = mongoDatabase.GetCollection<User>(
            recipesDatabaseSettings.Value.UserCollectionName);
    }

    public async Task<List<User>> GetAsync() =>
        await _userCollection.Find(_ => true).ToListAsync();

    public async Task<User?> GetAsync(string email) =>
        await _userCollection.Find(x => x.Email == email).FirstOrDefaultAsync();

    public async Task CreateAsync(User newUser) =>
        await _userCollection.InsertOneAsync(newUser);

    public async Task UpdateAsync(string email, User updatedUser) =>
        await _userCollection.ReplaceOneAsync(x => x.Email == email, updatedUser);

    public async Task RemoveAsync(string email) =>
        await _userCollection.DeleteOneAsync(x => x.Email == email);

    public async Task<bool> AuthValid(string email, DateTime authDatetime)
    {
        var user = await _userCollection.Find(x => x.Email == email && x.AuthValidSince <= authDatetime).FirstOrDefaultAsync();

        return user != null;
    }
}