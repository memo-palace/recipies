using Google.Apis.Auth;
using dotnet_server.Models;

namespace dotnet_server.Services;

public class AuthService
{
    private readonly IConfiguration _config;
    private readonly ILogger<AuthService> _logger;
    private readonly UserService _userService;

    public AuthService(IConfiguration config, ILogger<AuthService> logger, UserService userService)
    {
        _config = config;
        _logger = logger;
        _userService = userService;
    }

    public async Task<GoogleJsonWebSignature.Payload?> ValidateIdToken(string idToken)
    {
        var clientId = _config.GetValue<string>("GoogleAuth:ClientId")!;
        _logger.LogInformation(clientId);
        var validationSettings = new GoogleJsonWebSignature.ValidationSettings()
        {
            Audience = new List<string>() { clientId }
        };

        var payload = await GoogleJsonWebSignature.ValidateAsync(idToken);
        return payload;
    }

    public async Task<User> GetOrCreateUser(GoogleJsonWebSignature.Payload payload)
    {
        var user = _userService.GetAsync(payload.Email).Result;
        if (user == null)
        {
            user = new User()
            {
                Subject = payload.Subject,
                Email = payload.Email,
                Active = true,
                Created = DateTime.UtcNow,
                Modified = DateTime.UtcNow,
                AuthValidSince = DateTime.UtcNow
            };
            await _userService.CreateAsync(user);
        }
        else
        {
            user.Subject = payload.Subject;
            user.Modified = DateTime.UtcNow;
            await _userService.UpdateAsync(payload.Email, user);
        }

        return user;
    }
}