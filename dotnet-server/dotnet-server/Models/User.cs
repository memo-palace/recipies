using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace dotnet_server.Models;

public class User
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; } = "";
    [BsonElement("subject")]
    public string Subject { get; set; } = "";
    [BsonElement("email")]
    public string Email { get; set; } = "";
    [BsonElement("active")]
    public bool Active { get; set; }
    [BsonElement("created")]
    public DateTime Created { get; set; }
    [BsonElement("modified")]
    public DateTime Modified { get; set; }
    [BsonElement("lastChanged")]
    public DateTime AuthValidSince { get; set; }
}