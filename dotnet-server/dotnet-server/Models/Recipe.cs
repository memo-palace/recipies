using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace dotnet_server.Models;

public class Recipe
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; }
    [BsonElement("title")]
    public string? Title { get; set; }
    [BsonElement("ingredients")]
    public string[]? Ingredients { get; set; }
    [BsonElement("description")]
    public string? Description { get; set; }
    [BsonElement("created")]
    public DateTime Created { get; set; }
    [BsonElement("modified")]
    public DateTime Modified { get; set; }
}