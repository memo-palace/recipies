using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

using dotnet_server.Services;

namespace dotnet_server.Controllers;

[Route("api/auth")]
[ApiController]
public class AuthController : ControllerBase
{

    private readonly AuthService _authService;
    private readonly ILogger<AuthController> _logger;

    public AuthController(AuthService authService, ILogger<AuthController> logger)
    {
        _authService = authService;
        _logger = logger;
    }

    [HttpPost("login")]
    public async Task<ActionResult> Login(LoginRequest loginRequest)
    {
        if (string.IsNullOrEmpty(loginRequest.token))
        {
            return new BadRequestObjectResult("Missing required token");
        }
        Google.Apis.Auth.GoogleJsonWebSignature.Payload? payload = null;
        try
        {
            payload = await _authService.ValidateIdToken(loginRequest.token);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error validating id token");

            return new BadRequestObjectResult("Invalid credentials");
        }

        var user = await _authService.GetOrCreateUser(payload!);

        var claims = new List<Claim>
        {
            new Claim(ClaimTypes.Name, user.Email),
            new Claim("LastChanged", DateTime.UtcNow.ToString())
        };

        var claimsIdentity = new ClaimsIdentity(
            claims, CookieAuthenticationDefaults.AuthenticationScheme);

        var authProperties = new AuthenticationProperties
        {
            AllowRefresh = true,
            // Refreshing the authentication session should be allowed.

            ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(60),
            // The time at which the authentication ticket expires. A 
            // value set here overrides the ExpireTimeSpan option of 
            // CookieAuthenticationOptions set with AddCookie.

            IsPersistent = loginRequest.rememberMe,
            // Whether the authentication session is persisted across 
            // multiple requests. When used with cookies, controls
            // whether the cookie's lifetime is absolute (matching the
            // lifetime of the authentication ticket) or session-based.

            //IssuedUtc = <DateTimeOffset>,
            // The time at which the authentication ticket was issued.

            //RedirectUri = <string>
            // The full path or absolute URI to be used as an http 
            // redirect response value.
        };

        await HttpContext.SignInAsync(
            CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties);

        _logger.LogInformation("User {Email} logged in at {Time}.",
            user.Email, DateTime.UtcNow);

        var loginResponse = new LoginResponse()
        {
            success = true,
        };

        return new OkObjectResult(loginResponse);
    }

    [HttpPost("logout")]
    public async Task Logout()
    {
        await HttpContext.SignOutAsync(
            CookieAuthenticationDefaults.AuthenticationScheme);
    }
}

public sealed class LoginRequest
{
    public string? token { get; set; }
    public bool rememberMe { get; set; } = false;
}

public sealed class LoginResponse
{
    public bool success { get; set; }
}