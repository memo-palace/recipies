# Recipies

Web application for storing and sharing recipes

## Database

Set up mongodb in Docker

```
docker pull mongo
docker volume create recipes_mongodb_data
docker run --rm -p 127.0.0.1:27017:27017 -v recipes_mongodb_data:/data/db --name recipes_mongodb -d mongo
docker exec -it recipes_mongodb mongosh

use Recipes
db.createCollection('Recipes')
db.createCollection('Users')

Queries examples:

db.Recipes.find()
db.Users.find()
db.Users.find({"email": "myemail@gmail.com"})
```
